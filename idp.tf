resource "aws_cognito_user_pool_domain" "domain" {
  domain          = var.amazoncognito_name
  user_pool_id    = aws_cognito_user_pool.user_pool_email.id
}

/*
*
  creation of aws_cognito_user_pool with simple identification
*
*/
resource "aws_cognito_user_pool" "user_pool_email" {


#  depends_on = [module.lambda]
  name = "${var.app_name}-pool"
  alias_attributes           = ["email"]
  auto_verified_attributes   = ["email"]

  verification_message_template{
    default_email_option ="CONFIRM_WITH_LINK"
    email_subject_by_link = var.email_confirmation_subject
    email_message_by_link = var.email_confirmation_body
  }
  
  lambda_config {
    custom_message  = var.lambda_conf.function_name==null ? "":module.lambda[0].lambda_api_arn
  }
  password_policy {
    minimum_length = 6
    require_numbers = true
    require_lowercase = false
    require_symbols = false
    require_uppercase = false
    temporary_password_validity_days = 7
  }
}


#resource for lambda cognito trigger permission
resource "aws_lambda_permission" "get_blog" {
  count = var.lambda_conf.function_name==null ? 0:1
  depends_on = [aws_cognito_user_pool.user_pool_email,module.lambda]
  statement_id  = "AllowExecutionFromCognito"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_conf.function_name
  principal     = "cognito-idp.amazonaws.com"
  source_arn    = "${aws_cognito_user_pool.user_pool_email.arn}"
}


//this ressource for facebookgoogle
resource "aws_cognito_user_pool_client" "client_facebook_google" {
  depends_on = [aws_cognito_user_pool.user_pool_email]
  count =var.facebook_client_id != "" && var.apple_client_id  == "" && var.google_client_id!=""? 1:0
  name = "${var.app_name}-client-google"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id
  refresh_token_validity = 30

  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  allowed_oauth_flows_user_pool_client = var.google_client_id != "" && var.facebook_client_id != ""  ? true : false
  supported_identity_providers =  ["Google","Facebook"]
  callback_urls                =  var.callback_urls 
  logout_urls                  =  var.logout_urls 
  allowed_oauth_flows          = ["code"]
  allowed_oauth_scopes         = ["email", "openid", "profile","aws.cognito.signin.user.admin"]
}

//this ressource for facebookgoogleapple
resource "aws_cognito_user_pool_client" "client_facebook_google_apple" {
  depends_on = [aws_cognito_user_pool.user_pool_email]
  count =var.facebook_client_id != "" && var.apple_client_id  != "" && var.google_client_id!=""? 1:0
  name = "${var.app_name}-client-google-apple"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id
  refresh_token_validity = 30

  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  allowed_oauth_flows_user_pool_client = var.google_client_id != "" && var.facebook_client_id != "" && var.apple_client_id  != "" ? true : false
  supported_identity_providers =  ["Google","Facebook", "SignInWithApple"]
  callback_urls                =  var.callback_urls 
  logout_urls                  =  var.logout_urls 
  allowed_oauth_flows          = ["code"]
  allowed_oauth_scopes         = ["email", "openid", "profile","aws.cognito.signin.user.admin"]
}

//this ressource for google_facebook
resource "aws_cognito_identity_pool" "main_facebbok_google" {
  count = var.facebook_client_id != "" && var.apple_client_id  == "" && var.google_client_id!=""? 1:0
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id = aws_cognito_user_pool_client.client_facebook_google[0].id
    provider_name = aws_cognito_user_pool.user_pool_email.endpoint
    server_side_token_check = false

  }

}
//this ressource for google_facebook_apple
resource "aws_cognito_identity_pool" "main_facebbok_apple_google" {
  count = var.facebook_client_id != "" || var.apple_client_id != "" ? 1:0
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id = aws_cognito_user_pool_client.client_facebook_google_apple[0].id
    provider_name = aws_cognito_user_pool.user_pool_email.endpoint
    server_side_token_check = false

  }

}

//this ressource for cognito user pool (email Signin)
resource "aws_cognito_user_pool_client" "client" {
  depends_on = [aws_cognito_user_pool.user_pool_email]
  count = var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id == ""  ? 1:0
  name = "${var.app_name}-client"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id
  refresh_token_validity = 30
  #  read_attributes  = ["nickname"]
  #  write_attributes = ["nickname"]

  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  allowed_oauth_flows_user_pool_client = false
  supported_identity_providers =   ["COGNITO"]
  callback_urls                =  var.callback_urls 
  logout_urls                  =  var.logout_urls 
  allowed_oauth_flows          = []
  allowed_oauth_scopes         = []
}

resource "aws_cognito_identity_pool" "main" {
  count = var.facebook_client_id == "" && var.apple_client_id == "" ? 1:0
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id = aws_cognito_user_pool_client.client[0].id
    provider_name = aws_cognito_user_pool.user_pool_email.endpoint
    server_side_token_check = false
  }

}

data "aws_iam_policy_document" "auth_iam_role_policy" {
  statement {
    sid = ""
    effect = "Allow"
    actions = [
      "sts:AssumeRoleWithWebIdentity"]
    principals {
      identifiers = [
        "cognito-identity.amazonaws.com"]
      type = "Federated"
    }
  }
}
data "aws_iam_policy_document" "unauth_iam_role_policy" {
  statement {
    sid = ""
    effect = "Allow"
    actions = [
      "sts:AssumeRoleWithWebIdentity"]
    principals {
      identifiers = [
        "cognito-identity.amazonaws.com"]
      type = "Federated"
    }
  }
}

data "aws_iam_policy_document" "unauth_web_policy" {
  depends_on = [aws_s3_bucket.upload_bucket]
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}/public/*"
    ]
    effect = "Allow"
  }
  statement {
    actions = [
      "s3:GetObject"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}/protected/*"
    ]
    effect = "Allow"
  }

  statement {
    condition {
      test = "StringLike"
      values = [
        "public/",
        "public/*",
        "protected/",
        "protected/*"]
      variable = "s3:prefix"
    }
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}"
    ]
    effect = "Allow"

  }

}

data "aws_iam_policy_document" "auth_web_policy" {
  depends_on = [aws_s3_bucket.upload_bucket]
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}/public/*",
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}/protected/&{cognito-identity.amazonaws.com:sub}/*",
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}/private/&{cognito-identity.amazonaws.com:sub}/*"
    ]
    effect = "Allow"
  }
  statement {
    actions = [
      "s3:GetObject"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}/protected/*"
    ]
    effect = "Allow"
  }

  statement {
    condition {
      test = "StringLike"
      values = [
        "public/",
        "public/*",
        "protected/",
        "protected/*",
        "private/&{cognito-identity.amazonaws.com:sub}/",
        "private/&{cognito-identity.amazonaws.com:sub}/*"
      ]
      variable = "s3:prefix"
    }
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.upload_bucket.bucket}"
    ]
    effect = "Allow"

  }

}

resource "aws_iam_role" "auth_iam_role" {
  name = "cognito_auth_iam_role_${var.app_name}"
  assume_role_policy = data.aws_iam_policy_document.auth_iam_role_policy.json
}

resource "aws_iam_role" "unauth_iam_role" {
  name = "cognito_unauth_iam_role_${var.app_name}"
  assume_role_policy = data.aws_iam_policy_document.unauth_iam_role_policy.json
}

resource "aws_iam_role_policy" "web_iam_unauth_role_policy" {
  name = "web_iam_unauth_role_policy_${var.app_name}"
  role = aws_iam_role.unauth_iam_role.id
  policy = data.aws_iam_policy_document.unauth_web_policy.json
}

resource "aws_iam_role_policy" "web_iam_auth_role_policy" {
  name = "web_iam_auth_role_policy_${var.app_name}"
  role = aws_iam_role.auth_iam_role.id
  policy = data.aws_iam_policy_document.auth_web_policy.json
}

resource "aws_cognito_identity_pool_roles_attachment" "main_facebook_google" {
  count = var.facebook_client_id != "" && var.apple_client_id == "" && var.google_client_id != ""  ? 1:0

  identity_pool_id = aws_cognito_identity_pool.main_facebbok_google[0].id

  roles = {
    authenticated = aws_iam_role.auth_iam_role.arn
    unauthenticated = aws_iam_role.unauth_iam_role.arn
  }
}

resource "aws_cognito_identity_pool_roles_attachment" "main_simple" {
  count = var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id == ""  ? 1:0

  identity_pool_id =  aws_cognito_identity_pool.main[0].id

  roles = {
    authenticated = aws_iam_role.auth_iam_role.arn
    unauthenticated = aws_iam_role.unauth_iam_role.arn
  }
}

resource "aws_cognito_identity_pool_roles_attachment" "main_facebook_google_apple" {
  count = var.facebook_client_id != "" && var.apple_client_id != "" && var.google_client_id != ""  ? 1:0
  identity_pool_id = aws_cognito_identity_pool.main_facebbok_apple_google[0].id
  roles = {
    authenticated = aws_iam_role.auth_iam_role.arn
    unauthenticated = aws_iam_role.unauth_iam_role.arn
  }
}

