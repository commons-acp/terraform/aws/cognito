/*
*
check if google_client_id exsist and then create aws_cognito_identity_provider with google identification
*
*/

resource "aws_cognito_identity_provider" "google_identity_provider" {
  count = var.google_client_id == "" ? 0:1
  user_pool_id  = aws_cognito_user_pool.user_pool_email.id
  provider_name = "Google"
  provider_type = "Google"

  provider_details = {
    authorize_scopes = "email"
    client_id                     = var.google_client_id
    client_secret                 = var.google_client_secret
    attributes_url                = "https://people.googleapis.com/v1/people/me?personFields="
    attributes_url_add_attributes = "true"
    authorize_url                 = "https://accounts.google.com/o/oauth2/v2/auth"
    oidc_issuer                   = "https://accounts.google.com"
    token_request_method          = "POST"
    token_url                     = "https://www.googleapis.com/oauth2/v4/token"
  }

  attribute_mapping = {
    email    = "email"
    username = "sub"
    nickname = "name"
    picture = "picture"

  }
}

//this ressource for google
resource "aws_cognito_user_pool_client" "client_google" {
  depends_on = [aws_cognito_user_pool.user_pool_email]
  count =var.facebook_client_id == "" && var.apple_client_id  == "" && var.google_client_id!=""? 1:0
  name = "${var.app_name}-client-google"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id
  refresh_token_validity = 30

  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  allowed_oauth_flows_user_pool_client = var.google_client_id != "" ? true : false
  supported_identity_providers =  ["Google"]
  callback_urls                =  var.callback_urls 
  logout_urls                  =  var.logout_urls 
  allowed_oauth_flows          = ["code"]
  allowed_oauth_scopes         = ["email", "openid", "profile","aws.cognito.signin.user.admin"]
}

//this ressource for google
resource "aws_cognito_identity_pool" "main_google" {
  count = var.facebook_client_id == "" && var.apple_client_id  == "" && var.google_client_id!=""? 1:0
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id = aws_cognito_user_pool_client.client_google[0].id
    provider_name = aws_cognito_user_pool.user_pool_email.endpoint
    server_side_token_check = false

  }
}

resource "aws_cognito_identity_pool_roles_attachment" "main_google" {
  count = var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id != ""  ? 1:0
  identity_pool_id = aws_cognito_identity_pool.main_google[0].id

  roles = {
    authenticated = aws_iam_role.auth_iam_role.arn
    unauthenticated = aws_iam_role.unauth_iam_role.arn
  }
}
