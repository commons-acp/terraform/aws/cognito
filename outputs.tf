output "user_pool_arn" {
  value = aws_cognito_user_pool.user_pool_email.arn

}
output "user_pool_id" {
  value = aws_cognito_user_pool.user_pool_email.id
}
output "app_client_id_simple" {
  value =  var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id == ""  ?aws_cognito_user_pool_client.client[0].id :""
}
output "app_client_id_facebook" {
  value =  var.facebook_client_id != "" && var.apple_client_id == "" && var.google_client_id == ""  ?aws_cognito_user_pool_client.client_facebook[0].id:""
}
output "app_client_id_google" {
  value =  var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id != ""  ? aws_cognito_user_pool_client.client_google[0].id:""
}
output "app_client_id_apple" {
  value =  var.facebook_client_id == "" && var.apple_client_id != "" && var.google_client_id == "" ? aws_cognito_user_pool_client.client_apple[0].id:""
}
output "app_client_id_facebook_google_apple" {
  value =  var.facebook_client_id != "" && var.apple_client_id != "" && var.google_client_id != ""  ? aws_cognito_user_pool_client.client_facebook_google_apple[0].id:""
}
output "app_client_id_facebook_google" {
  value = var.facebook_client_id != "" && var.apple_client_id == "" && var.google_client_id != ""  ?aws_cognito_user_pool_client.client_facebook_google[0].id:""
}

output "idendity_pool_facebook_id" {
  value = var.facebook_client_id != "" && var.apple_client_id == "" && var.google_client_id == "" ?aws_cognito_identity_pool.main_facebbok[0].id:""
}

output "idendity_pool_google_id" {
  value = var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id != "" ?aws_cognito_identity_pool.main_google[0].id:""
}
output "idendity_pool_apple_id" {
  value = var.facebook_client_id == "" && var.apple_client_id != "" && var.google_client_id == "" ?aws_cognito_identity_pool.main_apple[0].id:""
}
output "idendity_pool_apple_facebook_google_id" {
  value = var.facebook_client_id != "" && var.apple_client_id != "" && var.google_client_id != "" ?aws_cognito_identity_pool.main_facebbok_apple_google[0].id:""
}
output "idendity_pool_facebook_google_id" {
  value = var.facebook_client_id != "" && var.apple_client_id == "" && var.google_client_id != "" ?aws_cognito_identity_pool.main_facebbok_google[0].id :""
}
output "idendity_pool_simple" {
  value = var.facebook_client_id == "" && var.apple_client_id == "" && var.google_client_id == "" ?aws_cognito_identity_pool.main[0].id :""
}
output "upload_bucket_name" {
  value = aws_s3_bucket.upload_bucket.bucket
}

output "domain_name" {
  value = aws_cognito_user_pool_domain.domain.domain
}

output "oauth_procedure" {
  value = var.facebook_client_id == "" ? false : true
}
