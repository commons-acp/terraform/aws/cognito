/*
*
check if facebook_client_id exsist and then create aws_cognito_identity_provider with facebook identification
*
*/
resource "aws_cognito_identity_provider" "facebook_identity_provider" {
  count = var.facebook_client_id == "" ? 0 : 1
  provider_name = "Facebook"
  provider_type = "Facebook"
  user_pool_id  = aws_cognito_user_pool.user_pool_email.id


  provider_details  = {
    authorize_scopes = "email"
    //    api_version      = var.facebook_api_version
    client_id                     = var.facebook_client_id
    client_secret                 = var.facebook_client_secret
    attributes_url                = "https://graph.facebook.com/v6.0/me?fields="
    attributes_url_add_attributes = "true"
    authorize_url                 = "https://www.facebook.com/v6.0/dialog/oauth"
    token_request_method          = "GET"
    token_url                     = "https://graph.facebook.com/v6.0/oauth/access_token"

  }
  attribute_mapping = {
    email    = "email"
    name = "first_name"
    username = "id"
    family_name = "last_name"
    picture = "picture"
  }
}

//this ressource for FACEBOOK
resource "aws_cognito_user_pool_client" "client_facebook" {
  depends_on = [aws_cognito_user_pool.user_pool_email]
  count = var.facebook_client_id != "" && var.apple_client_id  == "" && var.google_client_id==""? 1:0
  name = "${var.app_name}-client-facebook"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id
  refresh_token_validity = 30

  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  allowed_oauth_flows_user_pool_client = var.facebook_client_id != ""  ? true : false
  supported_identity_providers =  ["Facebook"]
  callback_urls                =  var.callback_urls 
  logout_urls                  =  var.logout_urls 
  allowed_oauth_flows          = ["code"]
  allowed_oauth_scopes         = ["email", "openid", "profile","aws.cognito.signin.user.admin"]
}

//this ressource for facebook
resource "aws_cognito_identity_pool" "main_facebbok" {
  count = var.facebook_client_id != "" && var.apple_client_id  == "" && var.google_client_id==""? 1:0
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id = aws_cognito_user_pool_client.client_facebook[0].id
    provider_name = aws_cognito_user_pool.user_pool_email.endpoint
    server_side_token_check = false

  }

}

resource "aws_cognito_identity_pool_roles_attachment" "main_facebook" {
  count = var.facebook_client_id != "" && var.apple_client_id == "" && var.google_client_id == ""  ? 1:0
  identity_pool_id =aws_cognito_identity_pool.main_facebbok[0].id

  roles = {
    authenticated = aws_iam_role.auth_iam_role.arn
    unauthenticated = aws_iam_role.unauth_iam_role.arn
  }
}
