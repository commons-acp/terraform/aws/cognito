
#project variable
variable "app_name" {
  description = "Application name"
}
variable "aws_region" {
  default = "eu-west-1"
}
variable "upload_bucket_name" {
  description = ""
}
variable "allowed_origins" {
  type = list(string)
  default = ["*"]
}
variable "identity_pool_name" {
  description = ""
}
variable "amazoncognito_name" {
  description = ""
}
variable "email_confirmation_subject" {
  description = ""
}
variable "email_confirmation_body" {
  description = ""
}
variable "facebook_api_version" {
  default =""
  description = ""
}
variable "facebook_client_id" {
  default =""
  description = ""
}
variable "facebook_client_secret" {
  default =""
  description = ""
}
variable "callback_urls" {
  default = ["*"]
  type = list(string)
  description = ""
}
variable "logout_urls" {
  default = ["*"]
  type = list(string)
  description = ""
}
variable "google_client_id" {
  default = ""
  description = "google ID client "

}
// variables for apple provider
variable "apple_client_id" {
  default = ""
  description = "apple service ID"
}
variable "apple_team_id" {
  default = ""
  description = ""
}
variable "apple_key_id" {
  default = ""
  description = ""
}
variable "apple_private_key" {
  default = ""
  description = ""
}
variable "google_client_secret" {
  default = ""
  description = "Code secret du client"
}


variable "lambda_conf" {
  type = object({
    lambda_env = optional(map(string))
    lambda_role_arn=optional(string)
    function_name=optional(string)
    gitlab_package_url=optional(string)
    gitlab_api_token= optional(string)
    lambda_file_path=optional(string)
  })
  default = {}


  description = ""
}
