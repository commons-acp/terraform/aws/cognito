module "lambda" {

  count = var.lambda_conf.function_name==null ? 0:1
  source = "git::https://git@gitlab.com/commons-acp/terraform/aws/lambda?ref=v1.0.9"
  lambda_role_arn = var.lambda_conf.lambda_role_arn
  function_name = var.lambda_conf.function_name
  gitlab_package_url = var.lambda_conf.gitlab_package_url
  gitlab_api_token = var.lambda_conf.gitlab_api_token
  lambda_env = var.lambda_conf.lambda_env
  lambda_file_path = var.lambda_conf.lambda_file_path
}
