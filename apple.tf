/*
*
check if apple_client_id exsist and then create aws_cognito_identity_provider with apple identification
*
*/

resource "aws_cognito_identity_provider" "apple_identity_provider" {
  lifecycle {ignore_changes = [provider_details]}
  count = var.apple_client_id == "" ? 0:1
  provider_name = "SignInWithApple"
  provider_type = "SignInWithApple"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id

  provider_details = {
    authorize_scopes = "email name"
    client_id         = var.apple_client_id # This refers to "Apple services ID" in the AWS Console
    team_id           = var.apple_team_id
    key_id            = var.apple_key_id
    private_key       = var.apple_private_key
  }
  attribute_mapping = {
    email    = "email"
    name     = "name"
    username = "sub"

  }
}

//this ressource for apple
resource "aws_cognito_user_pool_client" "client_apple" {
  depends_on = [aws_cognito_user_pool.user_pool_email]
  count =var.facebook_client_id == "" && var.apple_client_id  != "" && var.google_client_id==""? 1:0
  name = "${var.app_name}-client-apple"
  user_pool_id = aws_cognito_user_pool.user_pool_email.id
  refresh_token_validity = 30

  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  allowed_oauth_flows_user_pool_client = var.apple_client_id != "" ? true : false
  supported_identity_providers =  ["SignInWithApple"]
  callback_urls                =  var.callback_urls 
  logout_urls                  =  var.logout_urls 
  allowed_oauth_flows          = ["code"]
  allowed_oauth_scopes         = ["email", "openid", "profile","aws.cognito.signin.user.admin"]
}

//this ressource for apple
resource "aws_cognito_identity_pool" "main_apple" {
  count = var.facebook_client_id == "" && var.apple_client_id  != "" && var.google_client_id==""? 1:0
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = false

  cognito_identity_providers {
    client_id = aws_cognito_user_pool_client.client_apple[0].id
    provider_name = aws_cognito_user_pool.user_pool_email.endpoint
    server_side_token_check = false

  }
}

resource "aws_cognito_identity_pool_roles_attachment" "main_apple" {
  count = var.facebook_client_id == "" && var.apple_client_id != "" && var.google_client_id == ""  ? 1:0

  identity_pool_id = aws_cognito_identity_pool.main_apple[0].id

  roles = {
    authenticated = aws_iam_role.auth_iam_role.arn
    unauthenticated = aws_iam_role.unauth_iam_role.arn
  }
}
